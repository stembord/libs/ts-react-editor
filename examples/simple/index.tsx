import "ts-polyfill";

import { default as React } from "react";
import { render } from "react-dom";
import { Editor } from "../../lib";

interface ISimpleProps {
  readOnly?: boolean;
}

interface ISimpleState {
  value: string;
}

class Simple extends React.Component<ISimpleProps, ISimpleState> {
  state: ISimpleState = {
    value: `const hello = () => {
  console.log("Hello, world!");
};

hello();`
  };

  onChange = (value: string) => {
    this.setState({ value });
  };

  render() {
    return (
      <Editor
        readOnly={this.props.readOnly}
        onChange={this.onChange}
        mode="javascript"
      >
        {this.state.value}
      </Editor>
    );
  }
}

render(
  <>
    <p>Read/Write</p>
    <Simple />
    <p>Read Only</p>
    <Simple readOnly />
  </>,
  document.getElementById("app")
);
