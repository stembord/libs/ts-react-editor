import * as React from "react";
import { default as AceEditor, IAceEditorProps } from "react-ace/lib/ace";

import "brace";

import "brace/mode/c_cpp";
import "brace/mode/coffee";
import "brace/mode/csharp";
import "brace/mode/css";
import "brace/mode/elixir";
import "brace/mode/golang";
import "brace/mode/handlebars";
import "brace/mode/html";
import "brace/mode/java";
import "brace/mode/javascript";
import "brace/mode/json";
import "brace/mode/markdown";
import "brace/mode/mysql";
import "brace/mode/python";
import "brace/mode/ruby";
import "brace/mode/rust";
import "brace/mode/sass";
import "brace/mode/xml";

import "brace/theme/github";

const NEW_LINE = /\r\n|\r|\n/;

export interface IEditorProps extends Exclude<IAceEditorProps, "value"> {
  fontSize: number;
  children: string;
  onChange: (children: string) => void;
}

export interface IEditorState {
  height: number;
}

export class Editor extends React.PureComponent<IEditorProps, IEditorState> {
  static defaultProps: Partial<IEditorProps> = {
    fontSize: 13,
    mode: "markdown",
    theme: "github",
    onChange: () => {},
    editorProps: {
      $blockScrolling: true
    }
  };
  state: IEditorState = {
    height: this.getHeight(this.props.children)
  };

  onChange = (children: string) => {
    if (this.props.readOnly !== true) {
      this.props.onChange(children);
      this.setState({ height: this.getHeight(children) });
    }
  };
  getHeight(children: string) {
    return (
      children.split(NEW_LINE).length *
      (this.props.fontSize * 1.0769230769230769)
    );
  }

  render() {
    return (
      <AceEditor
        {...this.props}
        height={`${this.state.height}px`}
        onChange={this.onChange}
        value={this.props.children}
      />
    );
  }
}
